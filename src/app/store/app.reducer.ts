import * as fromAuth from '../auth/store/auth.reducer';
import {AuthActions} from '../auth/store/auth.actions';
import {ActionReducerMap} from '@ngrx/store';

export interface AppState {
  auth: fromAuth.State;
}

export const appReducer: ActionReducerMap<AppState, AuthActions> = {
  auth: fromAuth.authReducer
};
