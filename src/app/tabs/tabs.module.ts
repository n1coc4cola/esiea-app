/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SideMenuComponent } from './side-menu/side-menu.component';
import { AboutComponent } from './side-menu/about/about.component';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { TabsPageRoutingModule } from './tabs-routing.module';
import { TabsComponent } from './tabs.component';
import { IonicModule } from '@ionic/angular';
import { RouterLink } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    RouterLink,
    IonicModule,
    CommonModule,
    TabsPageRoutingModule,
    NgOptimizedImage,
  ],
  declarations: [TabsComponent, SideMenuComponent, AboutComponent],
})
export class TabsPageModule {}
