/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentRef, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AboutComponent } from './about/about.component';
import * as fromApp from '../../store/app.reducer';
import { User } from '../../models/user.model';
import { Store } from '@ngrx/store';

// @ts-ignore
interface MenuItem<T extends ComponentRef = ComponentRef> {
  title: string;
  icon: string;
  route?: string;
  modal?: T;
}

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  user: User;

  menuItems: MenuItem[] = [
    {
      title: 'Paramètres',
      icon: 'settings-outline',
      route: '/settings',
    },
    {
      title: 'A propos',
      icon: 'information-circle-outline',
      modal: AboutComponent,
    },
  ];
  protected readonly String = String;

  constructor(
    private modalCtrl: ModalController,
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit() {
    this.store.select('auth').subscribe((authState) => {
      if (authState.user) {
        this.user = authState.user;
      }
    });
  }

  async openModal(component: any) {
    const modal = await this.modalCtrl.create({
      component: component,
      cssClass: 'stack-modal',
    });
    await modal.present();
  }
}
