/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as AuthActions from './../../../auth/store/auth.actions';
import * as fromApp from '../../../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Promo } from '../../../models/scheduleEvent';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {
  errorMessage: string;
  promoForm = new FormGroup({
    promo: new FormControl<Promo | null>(null, Validators.required),
  });
  promos: { label: string; value: string }[] = [
    {
      label: '1A LAVAL',
      value: '1l',
    },
    {
      label: '2A LAVAL',
      value: '2l',
    },
    {
      label: '3A LAVAL',
      value: '3l',
    },
    {
      label: '4A LAVAL',
      value: '4l',
    },
    {
      label: '5A LAVAL',
      value: '5l',
    },
    {
      label: '1A PARIS',
      value: '1p',
    },
    {
      label: '2A PARIS',
      value: '2p',
    },
    {
      label: '3A PARIS',
      value: '3p',
    },
    {
      label: '4A PARIS',
      value: '4p',
    },
    {
      label: '5A PARIS',
      value: '5p',
    },
  ];
  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.store.select('auth').subscribe((authState) => {
      if (authState.user?.prefs.schedule) {
        this.promoForm.patchValue({
          promo: authState.user?.prefs.schedule.promo,
        });
      }
    });
  }
  onSubmit() {
    if (this.promoForm.valid) {
      this.store.dispatch(
        new AuthActions.UpdatePreferencesStart({
          schedule: {
            promo: this.promoForm.value.promo!,
            custom: false,
          },
        })
      );
    }
  }
}
