import {Component, EventEmitter, Input, Output} from '@angular/core';

export interface SettingItem {
  title: string;
  description: string;
  icon: string;
  route?: string;
  function?: Function
}

@Component({
  selector: 'app-settings-list',
  templateUrl: './settings-list.component.html',
  styleUrls: ['./settings-list.component.scss'],
})
export class SettingsListComponent {
  @Input({required: true}) settingItems: SettingItem[];
  @Output() functionCalled = new EventEmitter<Function>();

  onFunctionCalled(functionName: Function) {
    this.functionCalled.emit(functionName);
  }
}
