/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import * as AuthActions from './../../auth/store/auth.actions';
import * as fromApp from '../../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  isLoading = true;
  errorMessage: string | null = null;
  passwordForm: FormGroup = new FormGroup({
    currentPassword: new FormControl(null, [
      Validators.required,
      Validators.minLength(8),
    ]),
    newPassword: new FormControl(null, [
      Validators.required,
      Validators.minLength(12),
    ]),
    newPasswordConfirmation: new FormControl(null, [
      Validators.required,
      this.isSamePassword.bind(this),
    ]),
  });

  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.store.select('auth').subscribe((authState) => {
      this.isLoading = authState.isLoading;
      this.errorMessage = authState.errorMessage;
    });
  }

  isSamePassword(control: AbstractControl): ValidationErrors | null {
    if (control.value && this.passwordForm) {
      if (control.value === this.passwordForm.value['newPassword']) {
        return null;
      }
    }
    return { notSamePasswords: true };
  }

  onChangePassword() {
    if (this.passwordForm.valid) {
      this.store.dispatch(
        new AuthActions.ChangePasswordStart({
          currentPassword: this.passwordForm.value['currentPassword'],
          newPassword: this.passwordForm.value['newPassword'],
        })
      );
      this.passwordForm.reset();
    }
  }
}
