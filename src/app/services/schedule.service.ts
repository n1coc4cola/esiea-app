/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Promo, ScheduleEvent } from '../models/scheduleEvent';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// @ts-ignore
import ICAL from 'ical.js';

@Injectable({ providedIn: 'root' })
export class ScheduleService {
  dateSelected: BehaviorSubject<Date> = new BehaviorSubject<Date>(new Date());

  constructor(private _http: HttpClient) {}

  /**
   * Retrieves student schedule for a given promo.
   *
   * @param {Promo} promo - The promotional group for which to fetch the schedule events.
   * @returns {Observable<ScheduleEvent[]>} - An observable that emits an array of ScheduleEvent objects.
   *
   * @throws {Error} If there is an error while fetching or processing the schedule data.
   *
   */
  getEvents(promo: Promo): Observable<ScheduleEvent[]> {
    let URL = '/assets/ics/1l.ics';
    switch (promo) {
      case '1l':
        URL = '/assets/ics/1l.ics';
        break;
      case '2l':
        URL = '/assets/ics/2l.ics';
        break;
      case '3l':
        URL = '/assets/ics/3l.ics';
        break;
      case '4l':
        URL = '/assets/ics/4l.ics';
        break;
      case '5l':
        URL = '/assets/ics/5l.ics';
        break;
      case '1p':
        URL = '/assets/ics/1l.ics';
        break;
      case '2p':
        URL = '/assets/ics/2l.ics';
        break;
      case '3p':
        URL = '/assets/ics/3l.ics';
        break;
      case '4p':
        URL = '/assets/ics/4l.ics';
        break;
      case '5p':
        URL = '/assets/ics/5l.ics';
        break;
    }

    return this._http.get(URL, { responseType: 'text' }).pipe<ScheduleEvent[]>(
      map((ICalData) => {
        const events: ScheduleEvent[] = [];
        const jCalData = ICAL.parse(ICalData);
        const comp = new ICAL.Component(jCalData);
        const vevents = comp.getAllSubcomponents('vevent');
        for (let vevent of vevents) {
          const event = new ICAL.Event(vevent);
          const summary = event.summary.split(' - ');
          const subject = summary[1];
          const room = event.location
            ? event.location.split(' - ')[0]
            : 'Dans le metaverse';
          const teacher = summary[2];
          events.push(
            new ScheduleEvent(
              subject,
              room,
              teacher,
              event.startDate.toJSDate(),
              event.endDate.toJSDate(),
              false
            )
          );
        }
        return events;
      })
    );
  }
}
