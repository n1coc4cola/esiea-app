/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { User } from '../../models/user.model';
import { Action } from '@ngrx/store';
import { Promo } from '../../models/scheduleEvent';

export const LOGIN_START = '[Auth] Login Start';
export const MS_LOGIN_START = '[Auth] Login MS Start';
export const AUTHENTICATE_SUCCESS = '[Auth] Login';
export const LOGOUT = '[Auth] Logout';
export const AUTO_LOGIN = '[Auth] Auto Login';
export const REGISTER_START = '[Auth] Register Start';
export const CHANGE_PASSWORD_START = '[Auth] Change Password Start';
export const CHANGE_PASSWORD_SUCCESS = '[Auth] Change Password';
export const CHANGE_EMAIL_START = '[Auth] Change Email Start';
export const CHANGE_EMAIL_SUCCESS = '[Auth] Change Email';
export const CHANGE_PSEUDO_START = '[Auth] Change Pseudo Start';
export const CHANGE_PSEUDO_SUCCESS = '[Auth] Change Pseudo';
export const RECOVER_PASSWORD_START = '[Auth] Recover Password Start';
export const RECOVER_PASSWORD_CHECK = '[Auth] Recover Password Check';
export const RECOVER_PASSWORD_SUCCESS = '[Auth] Recover Password';
export const UPDATE_PREFERENCES_START = '[Auth] Update Preferences Start';
export const UPDATE_PREFERENCES_SUCCESS = '[Auth] Update Preferences';
export const DELETE_ACCOUNT_START = '[Auth] Delete Account Start';
export const UPDATE_AVATAR_START = '[Auth] Update Avatar Start';
export const UPDATE_AVATAR_SUCCESS = '[Auth] Update Avatar';
export const PROCESS_FAIL = '[Auth] Process Fail';

export class AuthenticateSuccess implements Action {
  readonly type = AUTHENTICATE_SUCCESS;

  constructor(
    public payload: {
      user: User;
      redirect: boolean;
    }
  ) {}
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class LoginStart implements Action {
  readonly type = LOGIN_START;
  constructor(public payload: { email: string; password: string }) {}
}

export class MSLoginStart implements Action {
  readonly type = MS_LOGIN_START;
}

export class RegisterStart implements Action {
  readonly type = REGISTER_START;
  constructor(
    public payload: { pseudo: string; email: string; password: string }
  ) {}
}

export class AutoLogin implements Action {
  readonly type = AUTO_LOGIN;
}

export class ChangePasswordStart implements Action {
  readonly type = CHANGE_PASSWORD_START;
  constructor(
    public payload: { currentPassword: string; newPassword: string }
  ) {}
}

export class ChangePasswordSuccess implements Action {
  readonly type = CHANGE_PASSWORD_SUCCESS;
}

export class ChangeEmailStart implements Action {
  readonly type = CHANGE_EMAIL_START;
  constructor(public payload: { newEmail: string; password: string }) {}
}

export class ChangeEmailSuccess implements Action {
  readonly type = CHANGE_EMAIL_SUCCESS;
  constructor(
    public payload: {
      user: User;
    }
  ) {}
}

export class ChangePseudoStart implements Action {
  readonly type = CHANGE_PSEUDO_START;
  constructor(
    public payload: {
      pseudo: string;
    }
  ) {}
}

export class ChangePseudoSuccess implements Action {
  readonly type = CHANGE_PSEUDO_SUCCESS;
  constructor(
    public payload: {
      user: User;
    }
  ) {}
}

export class RecoverPasswordStart implements Action {
  readonly type = RECOVER_PASSWORD_START;
  constructor(
    public payload: {
      email: string;
    }
  ) {}
}

export class RecoverPasswordCheck implements Action {
  readonly type = RECOVER_PASSWORD_CHECK;
  constructor(
    public payload: {
      userId: string;
      secret: string;
      password: string;
      passwordConfirmation: string;
    }
  ) {}
}

export class RecoverPasswordSuccess implements Action {
  readonly type = RECOVER_PASSWORD_SUCCESS;
  constructor() {}
}

export class UpdatePreferencesStart implements Action {
  readonly type = UPDATE_PREFERENCES_START;
  constructor(
    public payload: {
      schedule?: { promo: Promo; custom: boolean };
      avatar?: { href: URL; default: boolean };
    }
  ) {}
}

export class UpdatePreferencesSuccess implements Action {
  readonly type = UPDATE_PREFERENCES_SUCCESS;
  constructor(
    public payload: {
      user: User;
    }
  ) {}
}

export class DeleteAccountStart implements Action {
  readonly type = DELETE_ACCOUNT_START;

  constructor(
    public payload: {
      userId: string;
      password: string;
    }
  ) {}
}

export class UpdateAvatarStart implements Action {
  readonly type = UPDATE_AVATAR_START;

  constructor(public payload: { avatar: File; user: User }) {}
}

export class UpdateAvatarSuccess implements Action {
  readonly type = UPDATE_AVATAR_SUCCESS;

  constructor(
    public payload: {
      user: User;
    }
  ) {}
}

export class ProcessFail implements Action {
  readonly type = PROCESS_FAIL;

  constructor(public payload: string) {}
}

export type AuthActions =
  | AuthenticateSuccess
  | Logout
  | LoginStart
  | MSLoginStart
  | RegisterStart
  | ChangePasswordStart
  | ChangePasswordSuccess
  | ChangeEmailSuccess
  | ChangeEmailStart
  | ChangePseudoStart
  | ChangePseudoSuccess
  | RecoverPasswordStart
  | RecoverPasswordSuccess
  | RecoverPasswordCheck
  | UpdatePreferencesStart
  | UpdatePreferencesSuccess
  | DeleteAccountStart
  | UpdateAvatarStart
  | UpdateAvatarSuccess
  | ProcessFail;
