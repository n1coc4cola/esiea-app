/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { User } from '../../models/user.model';
import * as AuthActions from './auth.actions';

export interface State {
  user: User | null;
  errorMessage: string | null;
  isLoading: boolean;
}

const initialState: State = {
  user: null,
  errorMessage: null,
  isLoading: false,
};

export function authReducer(
  state: State = initialState,
  action: AuthActions.AuthActions
): State {
  switch (action.type) {
    case AuthActions.UPDATE_PREFERENCES_START:
    case AuthActions.RECOVER_PASSWORD_START:
    case AuthActions.CHANGE_PASSWORD_START:
    case AuthActions.DELETE_ACCOUNT_START:
    case AuthActions.UPDATE_AVATAR_START:
    case AuthActions.CHANGE_EMAIL_START:
    case AuthActions.REGISTER_START:
    case AuthActions.MS_LOGIN_START:
    case AuthActions.LOGIN_START:
      return {
        ...state,
        errorMessage: null,
        isLoading: true,
      };
    case AuthActions.UPDATE_PREFERENCES_SUCCESS:
    case AuthActions.UPDATE_AVATAR_SUCCESS:
    case AuthActions.CHANGE_PSEUDO_SUCCESS:
    case AuthActions.AUTHENTICATE_SUCCESS:
    case AuthActions.CHANGE_EMAIL_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        errorMessage: null,
        isLoading: false,
      };
    case AuthActions.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        errorMessage: null,
        isLoading: false,
      };
    case AuthActions.PROCESS_FAIL:
      return {
        ...state,
        user: null,
        errorMessage: action.payload,
        isLoading: false,
      };
    case AuthActions.LOGOUT:
      return {
        ...state,
        user: null,
        errorMessage: null,
        isLoading: false,
      };
    default:
      return state;
  }
}
