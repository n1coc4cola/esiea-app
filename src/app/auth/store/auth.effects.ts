/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { environment } from '../../../environments/environment';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ToastService } from '../../services/toast.service';
import { Preferences, User } from '../../models/user.model';
import * as AuthActions from './auth.actions';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Api } from '../../helpers/api';
import { switchMap, tap, of } from 'rxjs';

const handleError = (errorRes: any) => {
  let errorMessage = "Une erreur c'est produite.";
  switch (errorRes.type) {
    case 'user_invalid_credentials':
      errorMessage = 'Mot de passe invalide.';
      break;
    case 'general_rate_limit_exceeded':
      errorMessage = 'Limite de connexion atteinte.';
      break;
    case 'general_argument_invalid':
      errorMessage = 'Mot de passe de 8 caractère minimum.';
      break;
    case 'user_email_already_exists':
    case 'user_already_exists':
      errorMessage = "L'adresse email est déjà utilisé";
      break;
    case 'general_unknown':
      errorMessage = "Une erreur inconnue c'est produite.";
      break;
    case 'user_invalid_token':
      errorMessage = "Le token n'est pas valide.";
      break;
    case 'storage_file_type_unsupported':
      errorMessage = "L'extension du fichier n'est pas autorisé.";
      break;
    case 'user_unauthorized':
      errorMessage = 'Permission insuffisante.';
      break;
    case 'document_already_exists':
      errorMessage = 'Erreur, fichier existant.';
      break;
    default:
      console.info('Error not referenced.');
      console.log(errorRes);
  }
  return new AuthActions.ProcessFail(errorMessage);
};

async function buildUser(user: User): Promise<User> {
  const res = await Api.storages().listFiles(
    environment.bucketId,
    undefined,
    user.$id
  );
  if (res.total > 0) {
    return {
      ...user,
      avatar: new URL(
        Api.storages().getFilePreview(environment.bucketId, user.$id, 64, 64)
          .href +
          '&random=' +
          Math.random()
      ),
    };
  } else {
    return { ...user, avatar: Api.avatars().getInitials() };
  }
}


// noinspection JSUnusedGlobalSymbols
@Injectable()
export class AuthEffects {

  msAuthLogin = createEffect(() =>
      this.actions$.pipe(
        ofType(AuthActions.MS_LOGIN_START),
        switchMap(() => {
           console.log("Generating OAuth");
           
           try {
            Api.accounts().updateSession('[SESSION_ID]').then(function (response) {
              console.log(response); // Success
            }, function (error) {
              console.log(error); // Failure
            });

             Api.accounts().createOAuth2Session(
                "microsoft",
                "http://localhost:8100/cacaproute",
                "http://localhost:8100/karaba",
            );
          } catch (error) {
            console.log("ECHEC!");
            console.log(error);
          }

          return of({ type: 'DUMMY' });
      })
    )
  );

  authLogin = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LOGIN_START),
      switchMap((authData: AuthActions.LoginStart) =>
        Api.accounts()
          .createEmailSession(authData.payload.email, authData.payload.password)
          .then(
            () => {
              return Api.accounts()
                .get()
                .then(
                  async (user) =>
                    new AuthActions.AuthenticateSuccess({
                      user: await buildUser(user),
                      redirect: true,
                    })
                );
            },
            (errorRes) => handleError(errorRes)
          )
        )
      )
  );

  authRedirect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.AUTHENTICATE_SUCCESS),
        tap(async (authSuccessAction: AuthActions.AuthenticateSuccess) => {
          if (authSuccessAction.payload.redirect) {
            await this.router.navigate(['/tabs', 'tab1'], { replaceUrl: true });
          }
        })
      ),
    { dispatch: false }
  );

  autologin = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.AUTO_LOGIN),
      switchMap(async () =>
        Api.accounts()
          .get()
          .then(
            (session) => {
              if (!session || !session.email) {
                return { type: 'DUMMY' };
              }
              return Api.accounts()
                .get()
                .then(async (user: User) => {
                  return new AuthActions.AuthenticateSuccess({
                    user: await buildUser(user),
                    redirect: false,
                  });
                });
            },
            () => {
              return { type: 'DUMMY' };
            }
          )
      )
    )
  );

  logOut = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LOGOUT),
      switchMap(() =>
        Api.accounts()
          .deleteSessions()
          .then(async () => {
            await this.router.navigate(['/login']);
            return { type: 'DUMMY' };
          })
      )
    )
  );

  changePassword = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.CHANGE_PASSWORD_START),
      switchMap((registerData: AuthActions.ChangePasswordStart) =>
        Api.accounts()
          .updatePassword(
            registerData.payload.newPassword,
            registerData.payload.currentPassword
          )
          .then(
            async () => {
              await this.toastService.presentToast(
                'Mot de passe modifié avec succès.'
              );
              return new AuthActions.ChangePasswordSuccess();
            },
            (errorRes) => handleError(errorRes)
          )
      )
    )
  );

  changePseudo = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.CHANGE_PSEUDO_START),
      switchMap((registerData: AuthActions.ChangePseudoStart) =>
        Api.accounts()
          .updateName(registerData.payload.pseudo)
          .then(
            async (user: User) => {
              return new AuthActions.ChangePseudoSuccess({
                user: await buildUser(user),
              });
            },
            (e) => {
              console.log(e);
              return { type: 'DUMMY' };
            }
          )
      )
    )
  );

  changeEmail = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.CHANGE_EMAIL_START),
      switchMap((registerData: AuthActions.ChangeEmailStart) =>
        Api.accounts()
          .updateEmail(
            registerData.payload.newEmail,
            registerData.payload.password
          )
          .then(
            (user: User) => {
              return new AuthActions.ChangeEmailSuccess({ user });
            },
            (errorRes) => handleError(errorRes)
          )
      )
    )
  );

  register = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.REGISTER_START),
      switchMap(async (registerData: AuthActions.RegisterStart) =>
        Api.accounts()
          .create(
            'unique()',
            registerData.payload.email,
            registerData.payload.password,
            registerData.payload.pseudo
          )
          .then(
            () =>
              new AuthActions.LoginStart({
                email: registerData.payload.email,
                password: registerData.payload.password,
              }),
            (errorRes) => handleError(errorRes)
          )
      )
    )
  );

  sendRecoverPasswordLink = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.RECOVER_PASSWORD_START),
      switchMap(async (recoveryData: AuthActions.RecoverPasswordStart) =>
        Api.accounts()
          .createRecovery(
            recoveryData.payload.email,
            'http://localhost:4200/reset-password'
          )
          .then(
            () => ({ type: 'DUMMY' }),
            (errorRes) => handleError(errorRes)
          )
      )
    )
  );

  resetPassword = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.RECOVER_PASSWORD_CHECK),
      switchMap(async (resetData: AuthActions.RecoverPasswordCheck) =>
        Api.accounts()
          .updateRecovery(
            resetData.payload.userId,
            resetData.payload.secret,
            resetData.payload.password,
            resetData.payload.password
          )
          .then(
            () => {
              return new AuthActions.RecoverPasswordSuccess();
            },
            (errorRes) => handleError(errorRes)
          )
      )
    )
  );

  updatePreferences = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.UPDATE_PREFERENCES_START),
      switchMap(async (prefData: AuthActions.UpdatePreferencesStart) =>
        Api.accounts()
          .getPrefs()
          .then((userPref: Preferences) =>
            Api.accounts()
              .updatePrefs({
                ...userPref,
                ...prefData.payload,
              })
              .then(
                async (user: User) =>
                  new AuthActions.UpdatePreferencesSuccess({
                    user: await buildUser(user),
                  }),
                (errorRes) => handleError(errorRes)
              )
          )
      )
    )
  );

  deleteAccount = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.DELETE_ACCOUNT_START),
      switchMap(async (userData: AuthActions.DeleteAccountStart) =>
        Api.functions()
          .createExecution(
            environment.deleteAccountFunction,
            JSON.stringify({
              userId: userData.payload.userId,
              password: userData.payload.password,
            })
          )
          .then(
            (res) => {
              if (res.statusCode !== 200) {
                return handleError(
                  res.response
                    ? JSON.parse(res.response)
                    : { type: 'general_unknown' }
                );
              } else {
                this.router.navigate(['/login']);
                return { type: 'DUMMY' };
              }
            },
            (errorRes) => handleError(errorRes)
          )
      )
    )
  );

  updateAvatar = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.UPDATE_AVATAR_START),
      switchMap(async (avatarData: AuthActions.UpdateAvatarStart) => {
        const res = await Api.storages().listFiles(
          environment.bucketId,
          undefined,
          avatarData.payload.user.$id
        );
        if (res.total > 0) {
          await Api.storages()
            .deleteFile(environment.bucketId, avatarData.payload.user.$id)
            .catch((error) => handleError(error));
        }
        return Api.storages()
          .createFile(
            environment.bucketId,
            avatarData.payload.user.$id,
            avatarData.payload.avatar
          )
          .then(
            async () =>
              new AuthActions.UpdateAvatarSuccess({
                user: await buildUser(avatarData.payload.user),
              }),
            (error) => handleError(error)
          );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private toastService: ToastService
  ) {}
}
