import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as AuthActions from './../store/auth.actions';
import * as fromApp from '../../store/app.reducer';
import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup(
    {
      'email': new FormControl(null, [Validators.email, Validators.required]),
      'password': new FormControl(null, Validators.required)
    }
  );
  isLoading = true;
  messageError: string | null = null;

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.isLoading;
      this.messageError = authState.errorMessage;
      if (authState.errorMessage != null) {
        this.loginForm.get('password')?.setErrors({authError: true});
      } else {
        this.loginForm.get('password')?.setErrors(null);
      }
    });
  }

  onLogin() {
    if (!this.loginForm.valid) {
      return;
    }
    this.store.dispatch(new AuthActions.LoginStart({
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      }
    ));
  }

  handleMSConnect()
  {
    console.log("Clicked.");
    this.store.dispatch(new AuthActions.MSLoginStart());
  }
}
