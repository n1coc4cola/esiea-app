import { Router, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { Api } from '../../helpers/api';

@Injectable({ providedIn: 'root' })
export class AppGuard {
  constructor(private router: Router) {}

  canActivate(): Promise<boolean | UrlTree> {
    return Api.accounts()
      .get()
      .then(
        () => true,
        () => this.router.createUrlTree(['/login'])
      );
  }
}

@Injectable({ providedIn: 'root' })
export class LoginGuard {
  constructor(private router: Router) {}

  canActivate(): Promise<boolean | UrlTree> {
    return Api.accounts()
      .get()
      .then(
        () => this.router.createUrlTree(['/tabs/tab1']),
        () => true
      );
  }
}
