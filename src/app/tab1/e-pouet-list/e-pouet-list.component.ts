/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { EPouetService } from '../../services/e-pouet.service';
import { EPouet } from '../../models/e-pouet.model';

@Component({
  selector: 'app-e-pouet-list',
  templateUrl: './e-pouet-list.component.html',
  styleUrls: ['./e-pouet-list.component.scss'],
})
export class EPouetListComponent implements OnInit {
  ePouets: EPouet[] = [];

  constructor(private EPouetServices: EPouetService) {}

  ngOnInit() {
    this.EPouetServices.getPouets().then((res) => {
      this.ePouets = res;
    });
    this.EPouetServices.realTimePouet.subscribe((res) => {
      if (res) {
        this.ePouets = res;
      }
    });
  }
}
