/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export type Promo =
  | '1l'
  | '2l'
  | '3l'
  | '4l'
  | '5l'
  | '1p'
  | '2p'
  | '3p'
  | '4p'
  | '5p';
export class ScheduleEvent {
  public title: string;
  public room: string;
  public teacher: string;
  public startTime: Date;
  public endTime: Date;
  public allDay: boolean;

  constructor(
    title: string,
    room: string,
    teacher: string,
    startTime: Date,
    endTime: Date,
    allDay: boolean
  ) {
    this.title = title;
    this.room = room;
    this.teacher = teacher;
    this.startTime = startTime;
    this.endTime = endTime;
    this.allDay = allDay;
  }
}
