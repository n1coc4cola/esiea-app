/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent, CalendarMode, Step } from 'ionic2-calendar';
import { ScheduleService } from '../services/schedule.service';
import { ScheduleEvent } from '../models/scheduleEvent';
import * as fromApp from '../store/app.reducer';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.component.html',
  styleUrls: ['./tab2.component.scss'],
})
export class Tab2Component implements OnInit, OnDestroy {
  @ViewChild(CalendarComponent) myCalendar: CalendarComponent;
  scheduleSubscription: Subscription;
  eventSource: ScheduleEvent[] = [];
  calendar = {
    mode: 'day' as CalendarMode,
    currentDate: new Date(),
    startingDayWeek: 1,
    locale: 'fr-Fr',
    startHour: 7,
    endHour: 19,
    step: 15 as Step,
    noEventsLabel: "Aucun événements pour cette semaine.",
    dateFormatter: {
      formatDayViewHourColumn: function (date: Date) {
        const hour = new Date(date).getHours().toLocaleString('fr-FR');
        return hour + ':00';
      },
    },
  };

  constructor(
    private scheduleService: ScheduleService,
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit() {
    this.scheduleSubscription = this.scheduleService.dateSelected.subscribe(
      (date: Date) => {
        if (this.calendar.currentDate !== date) {
          this.calendar.currentDate = date;
        }
      }
    );

    this.store.select('auth').subscribe((authState) => {
      if (authState.user?.prefs.schedule) {
        this.scheduleService
          .getEvents(authState.user.prefs.schedule?.promo)
          .subscribe((events: ScheduleEvent[]) => {
            this.eventSource = events;
            this.myCalendar.loadEvents();
          });
      }
    });
  }

  onDateChanged(date: Date) {
    if (this.calendar.currentDate != date) {
      this.scheduleService.dateSelected.next(date);
    }
  }

  ngOnDestroy() {
    this.scheduleSubscription.unsubscribe();
  }

  getStringHour(date: Date) {
    const hour = date.getHours();
    const minutes = date.getMinutes();
    return hour + ':' + minutes;
  }
}
