import {RouterModule, Routes} from '@angular/router';
import {Tab2Component} from './tab2.component';
import {NgModule} from '@angular/core';


const routes: Routes = [
  {path: '', component: Tab2Component}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class Tab2RoutingModule {
}
