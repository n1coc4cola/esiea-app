/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endpoint: 'https://cloud.appwrite.io/v1',
  project: '649816d0962888dbe8e8',
  socialDatabase: '649a9c89b945e287a092',
  ePouetCollection: '649a9c939205d302adf7',
  voteFunction: '64a82969485ea6e5e5df',
  deleteAccountFunction: '64b3b3111d7a42051c6d',
  addLikeFunction: '64b8e3372f1abee57485',
  likesCollection: '649a9eeb712ced5b5d9f',
  bucketId: '64b96e12bcf244099e34',
  pouetBucket: '64bcf815cbca61a8d155',
  voteCollection: '64a8235f76db5003691f',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
