/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const sdk = require("node-appwrite");
const argon2 = require("argon2");

/*
  'req' variable has:
    'headers' - object with request headers
    'payload' - request body data as a string
    'variables' - object with function variables

  'res' variable has:
    'send(text, status)' - function to return text response. Status code defaults to 200
    'json(obj, status)' - function to return JSON response. Status code defaults to 200

  If an error is thrown, a response with code 500 will be returned.
*/

module.exports = async function (req, res) {
  const client = new sdk.Client();
  const users = new sdk.Users(client);
  const databases = new sdk.Databases(client);
  const payload = JSON.parse(req.payload ?? "{}");
  const socialDatabase = "649a9c89b945e287a092";
  const pouetCollection = "649a9c939205d302adf7";

  const userId = payload.userId;
  const password = payload.password;

  if (userId === undefined || users === null) {
    res.json(
      {
        message: "UserID is missing.",
        code: "400",
        type: "missing_userID",
      },
      400
    );
  }

  if (password === undefined || password === null) {
    res.json(
      {
        message: "Password is missing.",
        code: "400",
        type: "missing_password",
      },
      400
    );
  }

  if (
    !req.variables["APPWRITE_FUNCTION_ENDPOINT"] ||
    !req.variables["APPWRITE_FUNCTION_API_KEY"]
  ) {
    console.warn(
      "Environment variables are not set. Function cannot use Appwrite SDK."
    );
  } else {
    client
      .setEndpoint(req.variables["APPWRITE_FUNCTION_ENDPOINT"])
      .setProject(req.variables["APPWRITE_FUNCTION_PROJECT_ID"])
      .setKey(req.variables["APPWRITE_FUNCTION_API_KEY"]);
  }

  users.get(userId).then((userRes) => {
    verifyPassword(password, userRes.password).then(
      (response) => {
        if (!response) {
          return res.json(
            {
              message: "Invalid credentials.",
              code: "401",
              type: "user_invalid_credentials",
            },
            401
          );
        } else {
          databases
            .listDocuments(socialDatabase, pouetCollection, [
              sdk.Query.equal("user_id", userRes.$id),
            ])
            .then((documentsRes) => {
              for (let document of documentsRes.documents) {
                databases.deleteDocument(
                  socialDatabase,
                  pouetCollection,
                  document.$id
                );
              }
              users.delete(userRes.$id);
              return res.send("Ok", 200);
            });
        }
      },
      () => {
        res.json(
          {
            message: "An unknown error has occurred.",
            code: "500",
            type: "general_unknown",
          },
          500
        );
      }
    );
  });
};

async function verifyPassword(password, hash) {
  return await argon2.verify(hash, password);
}
