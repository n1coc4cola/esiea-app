import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'fr.esiea.app',
  appName: 'ESIEA',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
